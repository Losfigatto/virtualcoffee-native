# Spring Native

## Reference

Questo progetto è stato creato con:
* spring boot 3.0.5
* java 17
* Lombok
* Spring security

## Demo

![Architettura](doc/architetto.png)

L'idea è quella di creare un microservizio che riceve dei messaggi, li salva e ne mostra l'elenco alla richiesta.

Questo servizio viene protetto da uno IAM come Keycloak, e il microservizio valida il JWT Token dall'interno della rete e non dall'esterno.

Sono stati protetti gli url `private` con due ruoli: 
* SCOPE_read -> i metodi GET
* SCOPE_write -> i metodi POST

## Compilazione
Anche Spring si appoggia a GraalVM per la compilazione nativa, e questo lo fa grazie al plugin [AOT](https://docs.spring.io/spring-boot/docs/3.0.5/maven-plugin/reference/htmlsingle/#aot).

I comandi per la compilazione "tradizionale" sono sempre gli stessi. Anche quella per avviare:
```shell script
./mvnw spring-boot:run
```

A differenza di Quarkus, per generare un eseguibile nativo, bisogna avere GraalVM installato sulla macchina ed eseguire:

```shell script
./mvnw native:compile -Pnative
```

In alternativa, si può eseguire la compilazione e la creazione dell'immagine tutto in un container:

```
./mvnw spring-boot:build-image -Pnative
```

Una volta finito il processo, basta eseguire l'immagine con il comando:
```shell script
docker run --env EXTERNAL_KEYCLOAK_URL=https://www.external.eu --env INTERNAL_KEYCLOAK_URL=http://internal.int --rm -p 8080:8080 nativesecurity:0.0.1-SNAPSHOT 
```

# IMPORTANTI NOTE

Nell'endpoint `/private/list`, viene usata la reflection per serializzare la lista in uscita. Questo è uno di quei vincoli posti dalla AoT.

Per risolvere bisogna indicare al compilatore quali sono le classi che saranno oggetto di reflection, e questo lo si fa tramite una Annotation che viene messa su una classe:

```java
@RegisterReflectionForBinding(classes = {SimpleMessage.class, MessageSaved.class})
```

Se togliete questa annotazione e compilate e avviate l'applicativo in nativo, vedrete l'errore all'invocazione dell'endpoint.
