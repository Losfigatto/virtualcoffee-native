package com.noovle.nativesecurity.config;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.aot.hint.annotation.RegisterReflectionForBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.security.web.SecurityFilterChain;

import com.noovle.nativesecurity.dto.SimpleMessage;
import com.noovle.nativesecurity.model.MessageSaved;

@Configuration
@EnableWebSecurity
@RegisterReflectionForBinding(classes = {SimpleMessage.class, MessageSaved.class})
public class JWTSecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests(authz -> authz.requestMatchers(HttpMethod.GET, "/private/**").hasAuthority("SCOPE_read").requestMatchers(HttpMethod.POST, "/private/**").hasAuthority("SCOPE_write").anyRequest().not().authenticated()).oauth2ResourceServer().jwt().jwtAuthenticationConverter(getAuthenticationConverter());
        return http.build();
    }

    public Converter<Jwt, AbstractAuthenticationToken> getAuthenticationConverter() {
        return jwt -> new JwtAuthenticationToken(jwt, getAuthoritiesConverter().convert(jwt));
    }

    public Converter<Jwt, Collection<? extends GrantedAuthority>> getAuthoritiesConverter() {
        // This is a converter for roles as embedded in the JWT by a Keycloak server
        // Roles are taken from both realm_access.roles & resource_access.{client}.roles
        return jwt -> {
            final var realmAccess = (Map<String, Object>) jwt.getClaims().getOrDefault("realm_access", Map.of());
            final var realmRoles = (Collection<String>) realmAccess.getOrDefault("roles", List.of());

            final var resourceAccess = (Map<String, Object>) jwt.getClaims().getOrDefault("resource_access", Map.of());
            // List of roles assign on client

            // for each clientX
            // final var clientAccess = (Map<String, Object>) resourceAccess.getOrDefault("client", Map.of());
            // final var clientRoles = (Collection<String>) clientAccess.getOrDefault("roles", List.of());

            // return Stream.concat(realmRoles.stream(),clientRoles.stream()).map(SimpleGrantedAuthority::new).toList();

            return realmRoles.stream().map(SimpleGrantedAuthority::new).toList();
        };
    }

}
