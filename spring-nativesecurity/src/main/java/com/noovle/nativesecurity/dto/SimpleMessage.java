package com.noovle.nativesecurity.dto;

import lombok.Data;

@Data
public class SimpleMessage {

    private String user;
    private String message;
}
