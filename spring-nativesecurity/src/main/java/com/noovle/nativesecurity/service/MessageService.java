package com.noovle.nativesecurity.service;

import com.noovle.nativesecurity.dto.SimpleMessage;
import com.noovle.nativesecurity.model.MessageSaved;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class MessageService {

    private List<MessageSaved> messages = new ArrayList<>();

    public boolean saveMessage(SimpleMessage message){

        if(message!=null && StringUtils.hasText(message.getMessage()))
        {
            messages.add(new MessageSaved(message, Instant.now()));
            return true;
        }

        return false;
    }

    public List<MessageSaved> getMessages(){
        return Collections.unmodifiableList(messages);
    }
}
