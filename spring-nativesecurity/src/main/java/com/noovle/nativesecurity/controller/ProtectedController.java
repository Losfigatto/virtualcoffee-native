package com.noovle.nativesecurity.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.noovle.nativesecurity.dto.SimpleMessage;
import com.noovle.nativesecurity.service.MessageService;

@RestController
@RequestMapping("/private")
public class ProtectedController {

    @Autowired
    private MessageService service;

    @GetMapping(path = "/whoiam")
    public ResponseEntity getPrincipal(Principal principal) {
        return ResponseEntity.ok(principal.getName());
    }

    @GetMapping(path = "/list")
    public ResponseEntity getAllMessages() {
        return ResponseEntity.ok(service.getMessages());
    }

    @PostMapping(path = "/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity saveBody(@RequestBody SimpleMessage message) {

        return service.saveMessage(message) ? ResponseEntity.ok().build() : ResponseEntity.noContent().build();

    }

}
