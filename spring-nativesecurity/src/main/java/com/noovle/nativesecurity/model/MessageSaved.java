package com.noovle.nativesecurity.model;

import java.time.Instant;

import com.noovle.nativesecurity.dto.SimpleMessage;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MessageSaved {
    private SimpleMessage message;
    private Instant instant;
}
