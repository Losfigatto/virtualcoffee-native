package com.noovle.nativesecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NativesecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(NativesecurityApplication.class, args);
    }

}
