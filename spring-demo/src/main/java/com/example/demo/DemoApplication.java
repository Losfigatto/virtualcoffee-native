package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/m")
@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@GetMapping("/bmi/{mass}/{height}")
	/* @Produces(MediaType.TEXT_PLAIN) */
	public Double hello(@PathVariable String mass, @PathVariable String height) {
		return Double.valueOf(mass) / Math.pow(Double.valueOf(height), 2);
	}

}
