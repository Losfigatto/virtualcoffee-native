package com.noovle;

import javax.enterprise.event.Observes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.quarkus.runtime.StartupEvent;

@Path("/m")
public class GreetingResource {

    @GET
    @Path("/bmi/{mass}/{height}")
    @Produces(MediaType.TEXT_PLAIN)
    public Number hello(String mass, String height) {
        return Double.valueOf(mass) / Math.pow(Double.valueOf(height), 2);
    }

    void onStart(@Observes StartupEvent startup) {

        System.out.println("##############  PID: " + ProcessHandle.current().pid());

    }
}