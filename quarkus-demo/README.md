# quarkus-demo

Questa demo usa Quarkus, il Supersonic Subatomic Java Framework.

Se vuoi avere maggiori informazioni su Quarkus, il sito è: https://quarkus.io/ .

## Starter

Per iniziare il sito di riferimento è https://code.quarkus.io , l'initializer di questo framework.

Se vuoi iniziare da zero con la demo, clicca su 

https://code.quarkus.io/?g=com.noovle&a=demo-virtualcoffe&e=resteasy-reactive&e=smallrye-openapi

e potrai scaricare il pacchetto da zero.

## Avvia in modalità dev

Per far avviare la modalità dev basta eseguire il comando:
```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  In questa modalità, Quarkus mette a disposizione una dashboard di sviluppo che è disponibile all'indirizzo http://localhost:8080/q/dev/.

## Compilare e avviare l'applicativo

Per compilare il progetto si può usare il comando:
```shell script
./mvnw package
```
Questo produce un jar chiamato `quarkus-run.jar` nella cartella `target/quarkus-app/` che contiene solo le istruzioni di avvio perchè tutte le dipendenze vengono messe nella directory `target/quarkus-app/lib/`.

Per avviarla basta eseguire il comando `java -jar target/quarkus-app/quarkus-run.jar`.

Se invece si vuole un _über-jar_:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

Per avviarla basta eseguire il comando `java -jar target/*-runner.jar`.

## Lavoriamo con il nativo

Il plugin maven di quakus che permette di buildare in nativo si porta oot un profile `native`. 
Se si ha installato GraalVM sulla macchina allora basta eseguire il comando: 
```shell script
./mvnw package -Pnative
```

Altrimenti si può utilizzare la compilazione all'interno di un container:
```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

In entrambi i modi alla fine si otterrà un eseguibile `./target/demo-virtualcoffe-1.0.0-SNAPSHOT-runner`

## Note ulteriori
Nel progetto si ha anche una cartella `docker` che contiene dei Dockerfile utilizzabili per la creazione di immagini di produzione in base a come si vuole eseguire il servizio.

Inoltre esiste anche il comando che permette oltre alla compilazione anche quella di creare l'immagine docker.

```shell script
./mvnw package -Pnative -Dquarkus.container-image.build=true
```