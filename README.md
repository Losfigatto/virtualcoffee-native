# VirtualCoffee del 06/04/2023

Qui trovate le demo che ho usato nel virtual coffe. 

* quarkus-demo
* spring-demo

Per gli amanti di Spring che hanno paura a cambiare, ho fatto anche un progettino [spring-nativesecurity](spring-nativesecurity) basato su Spring Nativo con GraalVM con qualche interazione che si può avere in un microservizio.

